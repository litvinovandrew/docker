FROM asteris/apache-php-mongo:latest
MAINTAINER Andrew Lee <AndLeex@gmail.com>

RUN apt-get update && apt-get install -y \
 git \
 nano \
 mc \
 lynx

RUN a2enmod ssl

ADD MyVhost.conf /etc/apache2/sites-enabled/
ADD star_plexisoft_com.crt /etc/apache2/ssl/star_plexisoft_com.crt
ADD star_plexisoft_com.key /etc/apache2/ssl/star_plexisoft_com.key

VOLUME /parentwww

CMD ["/run.sh"]
